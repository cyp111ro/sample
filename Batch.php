<?php


class Batch
{

    public $consignments = [];


    /**
     * returns the consignments in the array
     * @return int
     */
    public function getNextId()
    {
        return count($this->consignments) + 1;
    }


    /**
     * startingNewBatch
     */
    public function startNewBatch()
    {
        $this->consignments = [];
    }

    /**
     * @param $consignmentType AncConsignment | RoyalMailConsignment
     * @param $name
     * @param $address
     */

    public function createConsignment($consignmentType, $name, $address)
    {
        if (class_exists($consignmentType)) {
            $this->consignments[] = (new $consignmentType($name, $address));
        }

    }

    /**
     * Sending all consignments
     * Resetting the array
     */
    public function endCurrentBatch()
    {
        foreach ($this->consignments as $consignment) {
            if ($consignment->consignmentType === 'AncConsignment') {
                (new FtpDataTransporter($consignment))->send();
            } else {
                (new RoyalMailConsignment($consignment))->send();
            }
        }
        //Clearing the batch for next day
        $this->consignments = [];

    }

    /**
     * Function created to be sure we do not start an already stared batch
     * @return int
     */
    public function hasConsignments()
    {
        return count($this->consignments);
    }

}