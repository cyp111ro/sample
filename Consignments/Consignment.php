<?php


class Consignment
{
    private $id;

    private $name;

    private $address;

    private $consignmentType;

    /**
     * Consignment constructor.
     * @param string $name
     * @param string $address
     */

    public function __construct($name, $address)
    {
        $this->name = $name;
        $this->address = $address;
        /**
         * Attempting to get the extended Class name e.g. AncConsignment
         * this will decide how that consignment will be sent
         */
        $this->consignmentType = get_class($this);
        $this->setNextId();
    }

    /**
     * Set the Consignment id
     * @param $id
     */
    public function setNextId()
    {
        $this->id++;
    }

    /**
     * return the Consignment id
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get file path for FTP
     */
    public function getFilePath()
    {
        return 'path of the saved file';
    }

    /**
     * @return string
     */
    public function getMailSubject()
    {
        return "Consignment ID {$this->getId()} Dear $this->name.";
    }

    /**
     * @return string
     */
    public function getMailBody()
    {
        return "Mail html body";
    }

}