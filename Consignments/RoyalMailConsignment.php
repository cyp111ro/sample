<?php


class RoyalMailConsignment extends Consignment
{
    /**
     * This algorithm can be requested from a config file as well
     * @var string
     */
    private $algorithm = 'ROYAL%dConsignment';

    /**
     * Override id with pattern
     * @return string
     */
    public function getId()
    {
        return sprintf($this->algorithm, $this->id());

    }

    /**
     * Sending itself by ftp
     */
    public function send()
    {
        (new FtpDataTransporter('RoyalMailConsignment'))->sendConsignment($this);
    }
}