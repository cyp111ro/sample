<?php


class AncConsignment extends Consignment
{

    /**
     * This algorithm can be requested from a config file as well
     * @var string
     */
    private $algorithm = 'ANC%dConsignment';

    /**
     * Override id with pattern the preferred way
     * @return string
     */
    public function getId()
    {
        return sprintf($this->algorithm, $this->id());

    }

    /**
     * @param Consignment $consignment
     */
    public function send(Consignment $consignment)
    {
        (new EmailDataTransporter('AncConsignment'))->sendConsignment($this);
    }
}