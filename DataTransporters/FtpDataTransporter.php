<?php


class FtpDataTransporter extends Transporter implements TransporterAbstract
{

    /**
     * Initiating the transporter
     * FtpDataTransporter constructor.
     * @param string $courier
     * @throws ErrorException
     */
    public function __construct($courier)
    {
        parent::__construct('ftp', $courier);
    }

    /**
     * @param Consignment $consignment
     * @return string
     */

    public function sendConsignment(Consignment $consignment)
    {
        if (ftp_put($this->ftpConnection, $consignment->getFilePath())) {
            return "successfully uploaded the consignment";
        } else {
            return "There was a problem while uploading the consignment {$consignment->getId()} \n";
        }

    }


}