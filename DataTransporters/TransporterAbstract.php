<?php

interface TransporterAbstract
{
    public function sendConsignment(Consignment $consignment);
}