<?php


class EmailDataTransporter extends Transporter implements TransporterAbstract
{

    /**
     * Initiating the transporter
     * FtpDataTransporter constructor.
     * @param string $courier
     * @throws ErrorException
     */
    public function __construct($courier)
    {
        parent::__construct('email', $courier);
    }


    /**
     * Gives the option to send only one
     * @param Consignment $consignment
     * @return string
     */
    public function sendConsignment(Consignment $consignment)
    {
        $this->emailConnection->Subject = $consignment->getMailSubject();
        $this->emailConnection->Body    = $consignment->getMailBody();
        $this->emailConnection->send();
    }

}