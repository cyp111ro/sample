<?php

/**
 * Class Transporter is the extended class where the connection
 * is taken care off depending on Courier and type of connection
 */
class Transporter
{


    public $emailConnection;

    public $ftpConnection;

    private $connectionType;

    private $courier;

    private $settings;

    /**
     * Transporter constructor.
     * @param $connectionType 'ftp'|'mail'
     * @param $courier 'AncConsignment'|'RoyalMailConsignment'
     * @throws ErrorException
     */
    public function __construct($connectionType, $courier)
    {
        $this->connectionType = $connectionType;
        $this->courier = $courier;
        $this->settings = $this->getConnectionDetails();
        $this->emailConnection = $this->getEmailConnection();
        $this->ftpConnection = $this->getFtpConnection();
    }


    /**
     * Setting mail as default
     * @return PHPMailer|resource
     * @throws ErrorException
     */
    public function getEmailConnection()
    {
        $url = $this->getSetting('url');
        $username = $this->getSetting('username');
        $password = $this->getSetting('password');
        $port = $this->getSetting('port');

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->Host = $url;
        $mail->SMTPAuth = true;
        $mail->Port = $port;
        $mail->Username = $username;
        $mail->Password = $password;
        return $mail;

    }


    /**
     * @return false|resource
     * @throws ErrorException
     */
    public function getFtpConnection()
    {
        $url = $this->getSetting('url');
        $username = $this->getSetting('username');
        $password = $this->getSetting('password');
        $connection = ftp_connect($url . $username . $password);
        if (ftp_login($connection, $username, $password)) {
            // Return the resource
            return $connection;
        } else {
            throw new ErrorException("Ftp connection error");
        }
    }


    /**
     * returns an array with settings from config.php
     * @return array
     */
    public function getConnectionDetails()
    {
        $config = include '../config.php';

        return $config[$this->courier][$this->connectionType];
    }

    /**
     * Returning the requested key and a default value
     * @param $settingKey
     * @return string
     */

    private function getSetting($settingKey)
    {
        if (array_key_exists($settingKey, $this->connection)) {
            return $this->settings[$settingKey];
        }
        return 'defaultValue';
    }


}