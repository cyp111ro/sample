<?php

/**
 * this file will return an array with FTP and mail settings
 */

return [
    'RoyalMailConsignment' => [
        'ftp' => [
            'url' => 'ftpUrl',
            'username' => 'ftpUsernme',
            'password' => 'ftpPassword',
        ],
        'mail' => [
            'host' => 'smtp.mailtrap.io',
            'port' => 2525,
            'username' => 'test',
            'password' => 'secret',
        ]
    ],
    'AncConsignment' => [
        'ftp' => [
            'url' => 'ftpUrlAnc',
            'username' => 'ftpUsernmeAnc',
            'password' => 'ftpPasswordAnc',
        ],
        'mail' => [
            'host' => 'smtp.mailtrap.io',
            'port' => 2525,
            'username' => 'test',
            'password' => 'secret',
        ]
    ]
];