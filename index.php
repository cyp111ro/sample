<?php
include_once "Consignments/AncConsignment.php";
include_once "Consignments/Consignment.php";
include_once "Consignments/RoyalMailConsignment.php";

include_once "DataTransporters/EmailDataTransporter.php";
include_once "DataTransporters/FtpDataTransporter.php";
include_once "DataTransporters/Transporter.php";
include_once "DataTransporters/TransporterAbstract.php";

$batch = new Batch();
$currentTime = time();
/**
 * Start Time and End time should be in the Batch class
 * Also a status according to the time should be in that class
 */
$start_time = '08:00:00';
$end_time = '18:00:00';

/**
 * If current time is greater than start time and no consignments
 */
if ($currentTime > strtotime($start_time) && $batch->hasConsignments() === 0) {
    $batch->startNewBatch();
}

/**
 * Anc Consignment
 */
    $batch->createConsignment('AncConsignment', 'James', '47 Naseby Road');

/**
 * Royal Mail consignment
 */
    $batch->createConsignment('RoyalMailConsignment', 'James', '47 Naseby Road');

/**
 * Ending batch
 */
if ($currentTime > strtotime($end_time)) {
    $batch->endCurrentBatch();
}




